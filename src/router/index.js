import { createRouter, createWebHistory } from 'vue-router'
import BannerShow from '../components/BannerShow.vue'
import CalendarioReservas from "../components/CalendarioReservas.vue";


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    
    {
      path: '/',
      name: 'home',
      component: BannerShow
    },
    {
      path: '/about',
      name: 'about',
    },
    {
      path: '/reservas',
      component: CalendarioReservas
    }
  ]
})

export default router
